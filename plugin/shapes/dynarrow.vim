"DRAW AN ARROW
nnoremap <leader>arh :call ArrowShowOptions()<cr>

nnoremap <leader>arl :call ArrowLoadSettings()<cr>
nnoremap <leader>ars :call ArrowShowSettings()<cr>
nnoremap <leader>arc :call ArrowSetArrow()<cr>
nnoremap <leader>ari :call ArrowToggleInsert()<cr>
nnoremap <leader>aro :call ArrowToggleOverwrite()<cr>
nnoremap <leader>arp :call ArrowTogglePadding()<cr>
nnoremap <leader>arb :call ArrowToggleBoundaries()<cr>

"let g:ARROW_TOKENS_*={'horizontal': "", 'vertical': "", 'arrowhead_up': "", 'arrowhead_down': "", 'arrowhead_left': "", 'arrowhead_right': "", 'corner_upright': "", 'corner_upleft': "", 'corner_downright': "", 'corner_downleft': "", 'cross_over': ""}

" ASCII based
let g:ARROW_TOKENS_DEFAULT={'horizontal': "-", 'vertical': "|", 'arrowhead_up': "^", 'arrowhead_down': "v", 'arrowhead_left': "<", 'arrowhead_right': ">", 'corner_upright': "#", 'corner_upleft': "#", 'corner_downright': "#", 'corner_downleft': "#", 'cross_over': ")"}

" UNICODE based
let g:ARROW_TOKENS_2={'horizontal': "\U2500", 'vertical': "\U2502", 'arrowhead_up': "\U25B3", 'arrowhead_down': "\U25BD", 'arrowhead_left': "\U25C1", 'arrowhead_right': "\U25B7", 'corner_upright': "\U2510", 'corner_upleft': "\U250C", 'corner_downright': "\U2518", 'corner_downleft': "\U2514", 'cross_over': ')'}


let g:ARROW_TOKENS={'horizontal': "\U2500", 'vertical': "\U2502", 'arrowhead_up': "\U25B3", 'arrowhead_down': "\U25BD", 'arrowhead_left': "\U25C1", 'arrowhead_right': "\U25B7", 'corner_upright': "\U2510", 'corner_upleft': "\U250C", 'corner_downright': "\U2518", 'corner_downleft': "\U2514", 'cross_over': ')'}

let g:arrow_insert=0
let g:overwrite_tokens=0
let g:arrow_boundaries=0
let g:arrow_padding=1

function! ArrowShowOptions()
	echo "'<leader><direction>' (normal): end arrow token"
	echo "'<leader>arl' (normal): load preset settings"
	echo "'<leader>ars' (normal): show arrow draw settings"
	echo "'<leader>arc' (normal): change arrow settings"
	echo "'<leader>ari' (normal): toggle arrow insert"
	echo "'<leader>aro' (normal): toggle overwrite chars"
	echo "'<leader>arb' (normal): toggle boundaries"
	echo "'<leader>arp' (normal): toggle padding"
endfunction

function! ArrowToggleBoundaries()
	if g:arrow_insert
		let g:arrow_boundaries=0
	else
		let g:arrow_boundaries=1	
	endif
	echo "Insert arrow: " . g:arrow_boundaries
endfunction

function! ArrowTogglePadding()
	if g:arrow_insert
		let g:arrow_padding=0
	else
		let g:arrow_padding=1	
	endif
	echo "Insert arrow: " . g:arrow_padding
endfunction

function! ArrowToggleInsert()
	if g:arrow_insert
		let g:arrow_insert=0
	else
		let g:arrow_insert=1	
	endif

	echo "Insert arrow: " . g:arrow_insert
endfunction

function! ArrowToggleOverwrite()
	if g:arrow_insert
		let g:overwrite_tokens=0
	else
		let g:overwrite_tokens=1	
	endif

	echo "Insert arrow: " . g:arrow_insert

endfunction

function! ArrowShowSettings()

	" Shows current settings 
	let l:ARROW_KEYS=keys(g:ARROW_TOKENS)

	echo "ARROW SETTINGS"
	"Reset arrow tokens
	for l:key in l:ARROW_KEYS
		let l:value=g:ARROW_TOKENS[l:key]	
		echo l:key . " = " l:value
	endfor

endfunction

function! ArrowLoadSettings()

	let l:ARROW_KEYS=keys(g:ARROW_TOKENS)
	
	let l:hasNext=1	
	while l:hasNext	
		" Create request string
		let b:requestString="Arrow setting: Default (0), Version_2 (1) "

		" Request and read input
		call inputsave()		
		let l:input=input(b:requestString)
		call inputrestore()
		echo "\n"
			
		" Check input	
		if l:input == 0
			for l:key in l:ARROW_KEYS
				let g:ARROW_TOKENS[l:key]=g:ARROW_TOKENS_DEFAULT[l:key]
			endfor
			let l:hasNext=0
		elseif l:input == 1
			for l:key in l:ARROW_KEYS
				let g:ARROW_TOKENS[l:key]=g:ARROW_TOKENS_2[l:key]
			endfor
			let l:hasNext=0
		else
			echo "Invalid input"
		endif
	endwhile

	echo "New tokens loaded" 

endfunction

function! ArrowSetSettings()
	
	" Loop through the keys and set values
	let g:ARROW_KEYS=keys(g:ARROW_TOKENS)
	for l:key in g:ARROW_KEYS
	
		let l:hasNext=1	
		while l:hasNext	
			" Create request string
			let b:requestString="Token for '" . l:key . "' : "

			" Request and read input
			call inputsave()		
			let l:input=input(b:requestString)
			call inputrestore()
			echo "\n"
			
			" Check input	
			if len(l:input) == 1
				let g:ARROW_TOKENS[l:key]=l:input
				let l:hasNext=0
			else
				echo "Enter a single token"
			endif
		endwhile
	endfor

	let l:hasNext=1
	while l:hasNext	
		" Create request string
		let b:requestString="APPEND (0) or INSERT (1)"

		" Request and read input
		call inputsave()		
		let l:input=input(b:requestString)
		call inputrestore()
		echo "\n"
			
		" Check input	
		if l:input == 1
			let arrow_insert=0
			let l:hasNext=0
		elseif l:input == 0
			let arrow_insert=1
			let l:hasNext=0
		else
			echo "Invalid option"
		endif
	endwhile
endfunction

function! DrawArrow(movement, end)

	if a:movement == 'UP'
		:call s:DrawArrowUp(a:end)
	elseif a:movement == 'DOWN'
		:call s:DrawArrowDown(a:end)
	elseif a:movement == 'LEFT'
		:call s:DrawArrowLeft(a:end)
	elseif a:movement == 'RIGHT'
		:call s:DrawArrowRight(a:end)
	endif
endfunction

function! <SID>DrawArrowUp(end)

	" Hold the command that gets build. The state of the cursor will no change before execution at the end
	let l:command="normal! "

	let l:cursorXPos_abs=col('.')-1
	let l:cursorYPos=line('.') " Line starts at 1
	let l:currentChar=strgetchar(getline(l:cursorYPos)[l:cursorXPos_abs:], 0)
	let l:previousChar=""
	if l:cursorXPos_abs != 0
		" Back up one char
		execute 'normal! h' 
		let l:tempXPosPrev_abs=col('.')-1
		let l:previousChar=strgetchar(getline(l:cursorYPos)[l:tempXPosPrev_abs:], 0)	
		" Restore	
		execute "normal! l" 
	endif

	let l:nextChar=""
	if l:cursorXPos_abs != col('$') - 2 "???
		" Next char
		execute 'normal! l' 
		let l:tempXPosNext_abs=col('.')-1
		let l:nextChar=strgetchar(getline(l:cursorYPos)[l:tempXPosNext_abs:], 0)	
		" Restore	
		execute "normal! h" 
	endif
	
	
	" SELECT CHAR
	if a:end
		let l:TOKEN=g:ARROW_TOKENS['arrowhead_up']
	elseif strgetchar(g:ARROW_TOKENS['horizontal'], 0) == l:previousChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downright']
	elseif strgetchar(g:ARROW_TOKENS['horizontal'], 0) == l:nextChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']	
	
	elseif strgetchar(g:ARROW_TOKENS['corner_downleft'], 0) == l:previousChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downright']	
	elseif strgetchar(g:ARROW_TOKENS['corner_upleft'], 0) == l:previousChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downright']		
	elseif strgetchar(g:ARROW_TOKENS['corner_downright'], 0) == l:nextChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']	
	elseif strgetchar(g:ARROW_TOKENS['corner_upright'], 0) == l:nextChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']	
	

	else
		let l:TOKEN=g:ARROW_TOKENS['vertical']
	endif
		
	" CHANGE TOKEN
	if g:arrow_insert
		if a:end && l:currentChar == strgetchar(g:ARROW_TOKENS['horizontal'], 0) || l:bLastChar 
			" TODO Small error (no insert mode for last char)
			let l:command=l:command . "a" . l:TOKEN
		else
			let l:command=l:command . "i" . l:TOKEN
		endif
		let l:command=l:command . "\<esc>"
	" Overwriting mode
	else
		" 52: unicode <space>
		if l:currentChar == '32' || g:overwrite_tokens 
			" If space replace by token	
			let l:command=l:command . "r" . l:TOKEN 
		elseif l:currentChar == strgetchar(g:ARROW_TOKENS['horizontal'], 0)
			let l:command=l:command . "r" . g:ARROW_TOKENS['cross_over'] 	
		else
			" Do nothing: only move to up
			let l:command="normal! "
		endif
	endif

	" PADDING
	if g:arrow_padding
		try
			:call PadLine('UP')
		catch
			let l:command="normal! "
		endtry
	endif		

	" MOVEMENT: Move char up
	let l:command=l:command . "k"

	"echo "CommU: " . l:command
	if l:command != "normal! "
		execute l:command
	endif

endfunction

function! <SID>DrawArrowDown(end)
	" Hold the command that gets build. The state of the cursor will no change before execution at the end
	let l:command="normal! "

	let l:cursorXPos_abs=col('.')-1
	let l:cursorYPos=line('.') " Line starts at 1
	let l:currentChar=strgetchar(getline(l:cursorYPos)[l:cursorXPos_abs:], 0)
	if l:cursorXPos_abs != 0
		" Back up one char
		execute 'normal! h' 
		let l:tempXPos_abs=col('.')-1
		let l:previousChar=strgetchar(getline(l:cursorYPos)[l:tempXPos_abs:], 0)	
		" Restore	
		execute "normal! l" 
	endif
	
	let l:previousChar=""
	if l:cursorXPos_abs != 0
		" Back up one char
		execute 'normal! h' 
		let l:tempXPosPrev_abs=col('.')-1
		let l:previousChar=strgetchar(getline(l:cursorYPos)[l:tempXPosPrev_abs:], 0)	
		" Restore	
		execute "normal! l" 
	endif

	let l:nextChar=""
	if l:cursorXPos_abs != col('$') - 2 "???
		" Next char
		execute 'normal! l' 
		let l:tempXPosNext_abs=col('.')-1
		let l:nextChar=strgetchar(getline(l:cursorYPos)[l:tempXPosNext_abs:], 0)	
		" Restore	
		execute "normal! h" 
	endif	

	" SELECT CHAR
	if a:end
		let l:TOKEN=g:ARROW_TOKENS['arrowhead_down']
	elseif strgetchar(g:ARROW_TOKENS['horizontal'], 0) == l:previousChar
		let l:TOKEN=g:ARROW_TOKENS['corner_upright']
	elseif strgetchar(g:ARROW_TOKENS['horizontal'], 0) == l:nextChar
		let l:TOKEN=g:ARROW_TOKENS['corner_upleft']
	
	elseif strgetchar(g:ARROW_TOKENS['corner_upright'], 0) == l:previousChar
		let l:TOKEN=g:ARROW_TOKENS['corner_upleft']	
	elseif strgetchar(g:ARROW_TOKENS['corner_upleft'], 0) == l:previousChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']		
	elseif strgetchar(g:ARROW_TOKENS['corner_upright'], 0) == l:nextChar
		let l:TOKEN=g:ARROW_TOKENS['corner_upleft']	
	elseif strgetchar(g:ARROW_TOKENS['corner_upleft'], 0) == l:nextChar
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']	
	

	else
		let l:TOKEN=g:ARROW_TOKENS['vertical']
	endif

	" CHANGE TOKEN
	if g:arrow_insert
		if a:end && l:currentChar == strgetchar(g:ARROW_TOKENS['horizontal'], 0) || l:bLastChar 
			" TODO Small error (no insert mode for last char)
			let l:command=l:command . "a" . l:TOKEN
		else
			let l:command=l:command . "i" . l:TOKEN
		endif
		let l:command=l:command . "\<esc>"
	" Overwriting mode
	else
		" 32: unicode <space>
		"echom "Curchar: " . l:currentChar
		if l:currentChar == '32' || g:overwrite_tokens 
			" If space replace by token	
			let l:command=l:command . "r" . l:TOKEN 
		elseif l:currentChar == strgetchar(g:ARROW_TOKENS['horizontal'], 0)
			let l:command=l:command . "r" . g:ARROW_TOKENS['cross_over'] 	
		else
			" Do nothing: only move to up
			let l:command="normal! "
		endif
	endif

	" PADDING
	if g:arrow_padding
		try
			:call PadLine('DOWN')
		catch
			let l:command="normal! "
		endtry
	endif		

	" MOVEMENT: Move char up
	let l:command=l:command . "j"	

	if l:command != "normal! "
		echo "CommDown: " . l:command
		execute l:command
	endif

endfunction

function! <SID>DrawArrowRight(end)

	" DATA{	
	" Hold the command that gets build. The state of the cursor will no change before execution at the end
	let l:command='normal! '

	let l:cursorXPos_abs=col('.')-1
	let l:cursorXPos_virt=virtcol('.')-1 " Column starts at 0
	let l:cursorYPos=line('.') " Line starts at 1
	
	let l:bLastChar=0
	let l:currentChar=strgetchar(getline(l:cursorYPos)[l:cursorXPos_abs:], 0)
	let l:previousChar=""
	let l:lineChars=virtcol('$')-1 " Amount of chars in line

	if l:lineChars == l:cursorXPos_virt + 1 " End of line reached
		let l:bLastChar=1
	endif
	
	let l:charUp=""
	if l:cursorYPos != 0
		if strchars(getline(l:cursorYPos-1)) >= strchars(getline(l:cursorYPos))
			" Char 1 line up
			execute 'normal! k' 
			let l:tempYPosUp=line('.')
			let l:tempXPos_abs=col('.')-1
			let l:charUp=strgetchar(getline(l:tempYPosUp)[l:tempXPos_abs:], 0)	
			" Restore	
			execute "normal! j" 
		endif
	endif

	let l:charDown=""
	if l:cursorYPos != line('$')
		if strchars(getline(l:cursorYPos+1)) >= strchars(getline(l:cursorYPos))
			" Char 1 line down
			execute 'normal! j' 
			let l:tempYPosDown=line('.')
			let l:tempXPos_abs=col('.')-1
			let l:charDown=strgetchar(getline(l:tempYPosDown)[l:tempXPos_abs:], 0)	
			
			" Restore	
			execute "normal! k" 
		endif
	endif

	" SET TOKEN
	if a:end
		let l:TOKEN=g:ARROW_TOKENS['arrowhead_right']
	elseif strgetchar(g:ARROW_TOKENS['vertical'], 0) == l:charUp
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']
	elseif strgetchar(g:ARROW_TOKENS['vertical'], 0) == l:charDown
		let l:TOKEN=g:ARROW_TOKENS['corner_upleft']

	elseif strgetchar(g:ARROW_TOKENS['corner_upleft'], 0) == l:charUp
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']	
	elseif strgetchar(g:ARROW_TOKENS['corner_upright'], 0) == l:charUp
		let l:TOKEN=g:ARROW_TOKENS['corner_downleft']		
	elseif strgetchar(g:ARROW_TOKENS['corner_downright'], 0) == l:charDown
		let l:TOKEN=g:ARROW_TOKENS['corner_upleft']	
	elseif strgetchar(g:ARROW_TOKENS['corner_downleft'], 0) == l:charDown
		let l:TOKEN=g:ARROW_TOKENS['corner_upleft']	
	
	else
		let l:TOKEN=g:ARROW_TOKENS['horizontal']
	endif
	"}END_DATA


	" CHANGE TOKEN
	if g:arrow_insert
		if a:end && l:currentChar == strgetchar(g:ARROW_TOKENS['horizontal'], 0)
			" TODO Small error (no insert mode for last char)
			let l:command=l:command . "a" . l:TOKEN
		else
			let l:command=l:command . "i" . l:TOKEN
		endif
		let l:command=l:command . "\<esc>"
	" Overwriting mode
	else
		if l:currentChar == '32' || g:overwrite_tokens 
			" If space replace by token	
			let l:command=l:command . "r" . l:TOKEN
		elseif l:currentChar == strgetchar(g:ARROW_TOKENS['vertical'], 0)
			let l:command=l:command . "r" . g:ARROW_TOKENS['cross_over'] 	
		endif
	endif

	" MOVEMENT: next char to the right
	if l:bLastChar && g:arrow_boundaries
		let l:command=":echo 'END OF LINE'"
	elseif l:bLastChar && !g:arrow_boundaries
		let l:command=l:command . "a\<space>\<esc>l"
	else
		let l:command=l:command . "l"
	endif

	"echo "CommR: " . l:command
	execute l:command

endfunction

function! <SID>DrawArrowLeft(end)

	"DATA

	" Hold the command that gets build. The state of the cursor will no change before execution at the end
	let l:command='normal! '

	let l:cursorXPos_abs=col('.')-1
	let l:cursorXPos_virt=virtcol('.')-1 " Column starts at 0
  	let l:cursorYPos=line('.') " Line starts at 1

	let l:bLastChar=0
	let l:currentChar=strgetchar(getline(l:cursorYPos)[l:cursorXPos_abs:], 0)
	let l:nextChar=""
	let l:lineChars=virtcol('$')-1 " Amount of chars in line

	let l:charUp=""
	if l:cursorYPos != 0
		if strchars(getline(l:cursorYPos-1)) >= strchars(getline(l:cursorYPos))
			" Char 1 line up
			execute 'normal! k' 
			let l:tempYPosUp=line('.')
			let l:tempXPos_abs=col('.')-1
			let l:charUp=strgetchar(getline(l:tempYPosUp)[l:tempXPos_abs:], 0)	
			" Restore	
			execute "normal! j" 
		endif
	endif

	let l:charDown=""
	if l:cursorYPos != line('$')
		if strchars(getline(l:cursorYPos+1)) >= strchars(getline(l:cursorYPos))
			" Char 1 line down
			execute 'normal! j' 
			let l:tempYPosDown=line('.')
			let l:tempXPos_abs=col('.')-1
			let l:charDown=strgetchar(getline(l:tempYPosDown)[l:tempXPos_abs:], 0)	
			
			" Restore	
			execute "normal! k" 
		endif
	endif


	" SET TOKEN
	if a:end
		let l:TOKEN=g:ARROW_TOKENS['arrowhead_left']
	elseif strgetchar(g:ARROW_TOKENS['vertical'], 0) == l:charUp
		let l:TOKEN=g:ARROW_TOKENS['corner_downright']
	elseif strgetchar(g:ARROW_TOKENS['vertical'], 0) == l:charDown
		let l:TOKEN=g:ARROW_TOKENS['corner_upright']	

	elseif strgetchar(g:ARROW_TOKENS['corner_upleft'], 0) == l:charUp
		let l:TOKEN=g:ARROW_TOKENS['corner_downright']	
	elseif strgetchar(g:ARROW_TOKENS['corner_upright'], 0) == l:charUp
		let l:TOKEN=g:ARROW_TOKENS['corner_downright']		
	elseif strgetchar(g:ARROW_TOKENS['corner_downright'], 0) == l:charDown
		let l:TOKEN=g:ARROW_TOKENS['corner_upright']	
	elseif strgetchar(g:ARROW_TOKENS['corner_downleft'], 0) == l:charDown
		let l:TOKEN=g:ARROW_TOKENS['corner_upright']	
	
	else
		let l:TOKEN=g:ARROW_TOKENS['horizontal']
	endif

	" CHANGE TOKEN	
	if g:arrow_insert
	" Insert mode
		let l:command=l:command . "i" . l:TOKEN
		let l:command=l:command . "\<esc>"
	" Overwriting mode
	else
		if l:currentChar == '32' || g:overwrite_tokens 
			" If space replace by token	
			let l:command=l:command . "r" . l:TOKEN 
		elseif l:currentChar == strgetchar(g:ARROW_TOKENS['vertical'], 0)
			let l:command=l:command . "r" . g:ARROW_TOKENS['cross_over'] 	
		else
			" Do nothing: only move to the right
			let l:command="normal! "
		endif
	endif

	" MOVEMENT: next char to the left
	if l:cursorXPos_virt == 0
		" If beginning do nothing
		let l:command=l:command . ":echo 'BEGINNING OF LINE'\<cr>"
	else	
		" Next char
		let l:command=l:command . 'h'
	endif		

	"echo "CommL: " . l:command
	execute l:command

endfunction
