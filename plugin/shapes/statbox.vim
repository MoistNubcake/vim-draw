nnoremap <leader>sbs :call StcBoxSetDim()<cr>
nnoremap <leader>sbh :call StcBoxHelp()<cr>
nnoremap <leader>sbo :call StcBoxToggleOverwrite()<cr>

let g:boxOverwrite=0
let g:STAT_BOX_DIM=[20, 8]

let g:STC_BOX_TOKENS={'boxcorner_upleft': "\U250C", 'boxcorner_upright': "\U2510", 'boxcorner_downright': "\U2518", 'boxcorner_downleft': "\U2514", 'box_horizontal': "\U2500", 'box_vertical': "\U2502"}

function! StcBoxToggleOverwrite()
	if g:boxOverwrite
		let g:boxOverwrite=0
	else
		let g:boxOverwrite=1	
	endif

	echo "Static box overwrite: " . g:boxOverwrite

endfunction

function! DrawStcBox()
	" Hold the command that gets build. The state of the cursor will no change before execution at the end
	let l:command="normal! "

	let l:cursorXPos_abs=col('.')-1
	let l:cursorYPos=line('.') " Line starts at 1
	let l:currentChar=strgetchar(getline(l:cursorYPos)[l:cursorXPos_abs:], 0)
	
	let l:horizontalUp=get(g:STC_BOX_TOKENS, 'boxcorner_upleft')
	let l:horizontalDown=get(g:STC_BOX_TOKENS, 'boxcorner_downleft')

	""" CREATE BOX STRINGS{
	let l:i=0

	while l:i < g:STAT_BOX_DIM[0] - 2
		let l:horizontalUp=l:horizontalUp . get(g:STC_BOX_TOKENS, 'box_horizontal')
		let l:horizontalDown=l:horizontalDown . get(g:STC_BOX_TOKENS, 'box_horizontal')
		let l:i=l:i + 1
	endwhile

	let l:horizontalUp=l:horizontalUp . get(g:STC_BOX_TOKENS, 'boxcorner_upright')
	let l:horizontalDown=l:horizontalDown . get(g:STC_BOX_TOKENS, 'boxcorner_downright')
	
	""" }END CREATE BOX STRINGS

	" Create command{
	" Create north border command
	let l:command=l:command . "v" . (g:STAT_BOX_DIM[0] - 1 ) . "lc"  . l:horizontalUp . "\<esc>"
	" Create west border
	" -3 Ticks: skip borders up and down + current line (already included)
	let l:ticksDown=g:STAT_BOX_DIM[1] - 3
	let l:command=l:command . "j\<c-v>" . l:ticksDown . "jc" . get(g:STC_BOX_TOKENS, 'box_vertical') . "\<esc>k"
	" Back up
	let l:backUpAmount = g:STAT_BOX_DIM[0] - 1
	let l:command=l:command	. l:backUpAmount . "h"
	" Create border east
	let l:command=l:command . "j\<c-v>" . l:ticksDown . "jc" . get(g:STC_BOX_TOKENS, 'box_vertical') . "\<esc>"
	" Create border south
	let l:moveDown=l:ticksDown+1
	let l:command=l:command . l:moveDown . "jv" . (g:STAT_BOX_DIM[0] - 1) . "lc"  . l:horizontalDown . "\<esc>"
	" }END_CREATE_COMMAND

	" Make box space {
	let l:currentPos=virtcol('.')
	let l:lineLength=virtcol('$')
	let l:tokensLeft=l:lineLength - l:currentPos
	let l:delAmount=0
	if l:tokensLeft < g:STAT_BOX_DIM[0]
		let l:delAmount=l:tokensLeft
	else
		let l:delAmount=g:STAT_BOX_DIM[0]
	endif

	let l:curPos=getcurpos()
	if virtcol(".") == 1
		if len(getline('.')) != 0
			execute "normal! " . l:delAmount . "i\<del>\<esc>" . g:STAT_BOX_DIM[0] . "i\<space>\<esc>"
		else
			execute "normal! " g:STAT_BOX_DIM[0] . "i\<space>\<esc>"
		endif
	else
		execute "normal! " . g:STAT_BOX_DIM[0] . "i\<space>\<esc>l" . l:delAmount . "i\<del>\<esc>l"
	endif

	let l:i=0
	try
		while l:i < g:STAT_BOX_DIM[1]
			:call PadLine('DOWN')
			execute "normal! j"
			let l:i = l:i + 1
		endwhile
	catch
		return 1	
	endtry

	call setpos('.', l:curPos)
	" } End make box space

	if l:command != "normal! "
		execute l:command
	endif


endfunction

function! StcBoxSetDim()
	let l:requestString=["Set X dimension: ", "Set Y dimension: "]

	let l:hasNext=1
	let l:counter=0
	
	while l:hasNext	
		call inputsave()
		let l:input=input(l:requestString[l:counter])
		call inputrestore()
		echo "\n"
		if l:input < 2
			echo "Invalid dimension"
		else
			let g:STAT_BOX_DIM[l:counter]=l:input
			let l:counter=l:counter + 1
		endif
		if l:counter > 1
			let l:hasNext=0
		endif
	endwhile
endfunction

function! StcBoxHelp()
	echo "Inputs a BOX with a set dimension"
	echo "Any arrow key will insert the box"
	echo ""
	echo "<leader>sbs: Set dimension"
	echo "<leader>sbo: Toggle overwrite"
endfunction
