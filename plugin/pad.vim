let g:padChar=" "

function! PadCurrentLine(amount)
	let l:command="normal! "
	let l:padChar="\<space>"
	if a:amount < 0
		echom "PadCurrentLine: invalid amount to pad"
		throw 1
	endif
	
	let l:lineLength=virtcol(".")
	let l:command =l:command . "$i"
	let l:padLength = a:amount
	while l:lineLength < l:padLength
		let l:command=l:command . l:padChar
		let l:lineLength=l:lineLength + 1
	endwhile
	let l:command=l:command . "\<esc>"
	echom l:command

	execute l:command
endfunction

function! PadLine(direction)

	" TODO minor: err out into err strem	
	" TODO average: reset cursorXPos?

	"" DESCRIPTION
	" Pad one line up from current line
	" Throws exception if end and start string exceed the padding length (1), if padding char is not 1 (2), if cursor is at beginneing of file (3), ifarg 1 (direction) is not UP || DOWN (4)
	"" END_DESCRIPTION

	"" DATA{
	let l:cursorYPos=line('.') " Line starts at 1
	let l:cursorXPos_virt=virtcol('.') " Amount of chars in current line
	let l:targetLineLength_virt=0
	let l:bLastLine=0
	
	if a:direction == 'UP'
		if l:cursorYPos != 0 " End of line reached
			let l:targetLineLength_virt=virtcol([l:cursorYPos-1,'$'])-1 " Amount of chars in next line
		endif
	elseif a:direction == 'DOWN'
		let l:lineNrs=line('$')
		if l:lineNrs == l:cursorYPos " End of file reached
			let l:bLastLine=1
		endif

		if ! l:bLastLine 
			let l:targetLineLength_virt=virtcol([l:cursorYPos+1,'$'])-1 " Amount of chars in next line 
		endif
	else
		echom "In GENERAL_FUNCTIONS.vim: FUNCTION PadLine(): Undefined direction: " . a:direction
		throw 4
	endif

	let l:command="normal! "
	let l:padString=""
	let l:absCharDiff= l:cursorXPos_virt - l:targetLineLength_virt
	let l:fixedCharsLength=len(g:startString) + len(g:endString)
	let l:padLength=l:absCharDiff - l:fixedCharsLength
	"" } END_DATA

	if l:absCharDiff < l:fixedCharsLength && l:fixedCharsLength != 0
		echo "Fixed length exceeds available padding size" 
		throw 1
	elseif len(g:padChar) != 1
		echo "Padding char is not 1"
		throw 2
	elseif l:cursorYPos == 1 && a:direction == 'UP'
		echo "BEGINNING OF FILE"
		throw 3
	endif

	" Build Command 
	if l:padLength > 0
		let l:padString=l:padString . g:startString	
		let l:i=0
		while l:i < l:padLength
			let l:padString=l:padString . g:padChar
				let l:i=l:i + 1
		endwhile

		let l:padString=l:padString . g:endString
		if a:direction == 'UP'
			let l:command=l:command . "k$a" " Move to end next line and insert mode
			let l:command=l:command . l:padString " Insert padding
			let l:command=l:command . "\<esc>j" " Return to normal mode
		elseif	a:direction == 'DOWN'
			if ! l:bLastLine
				let l:command=l:command . "j$a" " Move to end next line and insert mode
				let l:command=l:command . l:padString " Insert padding
				let l:command=l:command . "\<esc>k" " Return to normal mode
			else
				let l:command=l:command . "$a\<cr>" " Move to end next line and insert mode
				let l:command=l:command . l:padString " Insert padding
				let l:command=l:command . "\<esc>k" " Return to normal mode
			endif
		endif
	endif

	if l:command != "normal! "	
		execute l:command
	endif

endfunction
