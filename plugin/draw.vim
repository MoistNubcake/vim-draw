let b:movementInput='NOP'

let b:DRAW_MODES=['UNSET', 'ARROW', 'STBOX', 'PAD']
let b:drawMode=b:DRAW_MODES[0]

nnoremap draw :call SetDrawMode()<cr>
 
nnoremap <UP> :call DrawCall("UP", 0)<cr>
nnoremap <DOWN> :call DrawCall("DOWN", 0)<cr>
nnoremap <LEFT> :call DrawCall("LEFT", 0)<cr>
nnoremap <RIGHT> :call DrawCall("RIGHT", 0)<cr>

nnoremap <leader><UP> :call DrawCall("UP", 1)<cr>
nnoremap <leader><DOWN> :call DrawCall("DOWN", 1)<cr>
nnoremap <leader><LEFT> :call DrawCall("LEFT", 1)<cr>
nnoremap <leader><RIGHT> :call DrawCall("RIGHT", 1)<cr>

function! DrawCall(movement, end)
	" Calls the drawing function
	if b:drawMode == 'UNSET'
		echom 'Draw mode not set. Type "draw" in normal mode to select a mode'
	elseif b:drawMode == 'ARROW'
		:call DrawArrow(a:movement, a:end)
	elseif b:drawMode == 'STBOX'
		:call DrawStcBox()
    elseif b:drawMode == 'PAD'
        try
            :call PadLine(a:movement)
            if a:movement == 'UP'
                execute "normal! k"
            elseif a:movement == 'DOWN'
                execute "normal! j"
            endif
        catch

        endtry
	endif
endfunction

function! SetDrawMode()

	" Set the draw mode for drawing

	echom "Current draw mode is: " . b:drawMode
	let b:input='NOP'
	let b:hasNext=1
	while b:hasNext

		" Create request string
		let b:requestString="Select "
		for i in b:DRAW_MODES
			let b:drawIndex=index(b:DRAW_MODES , i)
			let b:requestString=b:requestString . i . " (" . b:drawIndex . "), "
		endfor
		let b:requestString=b:requestString . "RETURN (q): "

		" Request and read input
		call inputsave()
		let b:input=input(b:requestString)
		call inputrestore()
		echo "\n"

		for i in b:DRAW_MODES
			let b:drawIndex=index(b:DRAW_MODES, i)
			let b:drawIndexAsString=string(b:drawIndex) " Cast necessari for comparison to string (b:intput)		
			
			if b:input ==? b:drawIndexAsString
				let b:hasNext=0	
				let b:drawMode=b:DRAW_MODES[b:input]
				echo b:drawMode . " selected"
				echo "Enter '<leader>" . tolower(b:drawMode) . "' to show options (normal mode)"
			endif	
		endfor

		if b:input ==? 'q'
			let b:hasNext=0
			echo "Current draw mode is: " . b:drawMode
		endif

		if b:hasNext 	
			echo 'Undefined option (press q to quit)'
		endif
	endwhile
endfunction
